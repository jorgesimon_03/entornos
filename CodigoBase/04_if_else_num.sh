#!/bin/bash

read -p "Introduce un numero: " num
echo El numero es $num

echo -----------#-----------

##less than; menos que
if [ $num -lt 50 ]; then
	echo $num es menor que 50
fi

echo -----------#-----------

##greater than; mayor que
if [ $num -gt 50 ]; then
	echo $num es mayor que 50
fi

echo -----------#-----------

##greater equal; mayor o igual
if [ $num -ge 50 ]; then
	echo $num es mayor o igual que 50
fi

echo -----------#-----------

##less equal; menor o igual
if [ $num -le 50 ]; then
	echo $num es menor o igual que 50
fi

echo -----------#-----------

##equal; igual
if [ $num -eq 50 ]; then
	echo $num es igual que 50
fi

echo -----------#-----------

##not equal; no igual
if [ $num -ne 50 ]; then
	echo $num no es igual que 50
fi
