#!/bin/bash

alet=`echo $(($RANDOM%128))`

echo "Adivina el numero entre 0 y 128"

read -p "Escribe un numero: " num

while [ $num -ne $alet ]
do
	if [ $num -lt $alet ]; then
		echo "Mas alto que " $num
	else
		echo "Mas bajo que " $num
	fi

	read -p "Escribe un numero: " num

	clear
done
