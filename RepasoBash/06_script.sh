#!/bin/bash

read -p "Escribe un numero: " num1
read -p "Escribe un numero: " num2

if [ $num1 -lt $num2 ]; then
	echo $num1 "es menor que " $num2
fi

if [ $num1 -gt $num2 ]; then
	echo $num1 "es mayor que " $num2
fi

if [ $num1 -le $num2 ]; then
        echo $num1 "es menor/igual que " $num2
fi

if [ $num1 -ge $num2 ]; then
        echo $num1 "es mayor/igual que " $num2
fi

if [ $num1 -eq $num2 ]; then
	echo $num1 "es igual que " $num2
fi

if [ $num1 -ne $num2 ]; then
	echo $num1 "no es igual a " $num2
fi
