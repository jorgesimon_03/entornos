#!/bin/bash

numparam=3

if [ $# -ne $numparam ]; then
	echo "Numero incorrecto de parametros"
	echo "Por ejemplo:"
	echo "            ./11_script.sh 12 34 +"
else
	case $3 in
		+)
			let sum=$1+$2
			echo $sum
			;;
		-)
			echo $(($1-$2))
			;;
		/)
			echo $(($1/$2))
			echo "Resto" $(($1%$2))
			;;
		x)
			echo $(($1*$2))
			;;
		*)
			echo "[+] suma"
			echo "[-] resta"
			echo "[/] division"
			echo "[x] multiplicacion"
			;;
	esac
fi
