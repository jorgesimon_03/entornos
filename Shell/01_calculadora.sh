#!/bin/bash
echo [tecla]:
echo "       [1]= +   [2]= -   [3]= *   [4]= /"
echo "           SUMA    RESTA     MULT     DIV"
echo ¿Que operacion quieres hacer?
read operacion
echo

read -p "Operando 1: " op1
read -p "Operando 2: " op2

case $operacion in
	1)
		let suma=$op1+$op2
		echo La suma es: $suma;;
	2)
		let resta=$op1-$op2
		echo La resta es: $resta;;
	3)
		let mult=$op1*$op2
		echo La multiplicación es: $mult;;
	4)
		let div=$op1/$op2
		echo La división es: $div;;
esac		
